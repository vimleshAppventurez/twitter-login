import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func twitterSignIn(_ sender : UIButton) {
        TwitterManager.sharedInstance.signIn(controller: nil) { session, error in
            if session != nil {
                //Save User Data
            } else {
                print(error)
            }
        }
    }
}

