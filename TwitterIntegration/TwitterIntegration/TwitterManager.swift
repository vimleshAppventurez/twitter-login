import Foundation
import TwitterKit

class TwitterManager : NSObject {
    // MARK: - Singleton Instantiation
    private static let _sharedInstance: TwitterManager = TwitterManager()
    static var sharedInstance: TwitterManager {
        return _sharedInstance
    }
    
    private var apiKey          = "kCCsgzsEVg8xckp4WqAVVEK3x"
    private var apiSecretKey    = "JruY2wnx17QOOLgmu3yBNLCfTPFaNksCKAQASpW0qBMFqxSb0i"
    
    func start() {
        TWTRTwitter.sharedInstance().start(withConsumerKey: self.apiKey, consumerSecret: self.apiSecretKey)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    }
    
    func signIn(controller : UIViewController? = nil, completion : @escaping TWTRLogInCompletion) {
        TWTRTwitter.sharedInstance().logIn(with: controller, completion: completion)
    }
}
